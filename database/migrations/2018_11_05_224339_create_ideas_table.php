<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdeasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ideas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
            $table->string('description');
            $table->enum('status', ['andamento', 'concluido', 'nao iniciado', 'pausado'])->default('nao iniciado');
            $table->enum('project_type', ['TCC', 'ARTIGO', 'PROJETO CIENTIFICO','PESQUISA ACADEMICA']);
            $table->enum('private', ['PRIVATE', 'PUBLIC'])->default('PUBLIC');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ideas');
    }
}
