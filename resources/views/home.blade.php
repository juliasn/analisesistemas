@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="align-center">
            <img src="img/LogoTCCSolution.PNG" alt="">
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="align-center">
                <label>Busque por:</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="tipo_busca" id="titulo" value="titulo" checked>
                    <label class="form-check-label" for="titulo">Título</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="tipo_busca" id="categoria" value="categoria">
                    <label class="form-check-label" for="categoria">Categoria</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="tipo_busca" id="tipoprojeto" value="tipoprojeto">
                    <label class="form-check-label" for="tipoprojeto">Tipo Projeto</label>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="form-row">
                <div class="col-sm-12 my-1">
                    <input type="text" class="form-control" id="pesquisar" placeholder="Pesquisar" autofocus>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
