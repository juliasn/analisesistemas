@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
            <h3 class="border-bottom pb-2 mb-0">Meus Projetos</h3>
            <div class="container pt-3 border-bottom border-gray">
                <img src="" alt="">
                <a href="#" class="media-body pb-3 mb-0">Testando</a>
                <div class="clearfix"></div>
                <small class="small">Mussum Ipsum, cacilds vidis litro abertis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Casamenti... </small>
                <div class="clearfix"></div>
                <button class="btn btn-sm btn-danger mt-1 mb-3">Apagar</button>
            </div>
            <div class="container pt-3 border-bottom border-gray">
                <img src="" alt="">
                <a href="#" class="media-body pb-3 mb-0">Testando</a>
                <div class="clearfix"></div>
                <small class="small">Mussum Ipsum, cacilds vidis litro abertis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Casamenti... </small>
                <div class="clearfix"></div>
                <button class="btn btn-sm btn-danger mt-1 mb-3">Apagar</button>
            </div>
        </div>
        <div class="my-3 p-3 bg-white rounded shadow-sm">
            <h3 class="border-bottom pb-2 mb-0">Projetos em que participo</h3>
            <div class="container pt-3 border-bottom border-gray">
                <img src="" alt="">
                <a href="#" class="media-body pb-3 mb-0">Testando</a>
                <p class="small">@user</p>
                <div class="clearfix"></div>
                <small class="small">Mussum Ipsum, cacilds vidis litro abertis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Casamenti... </small>
            </div>
            <div class="container pt-3 border-bottom border-gray">
                <img src="" alt="">
                <a href="#" class="media-body pb-3 mb-0">Testando</a>
                <p class="small">@user</p>
                <div class="clearfix"></div>
                <small class="small">Mussum Ipsum, cacilds vidis litro abertis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Casamenti... </small>
            </div>
        </div>
    </div>
@endsection