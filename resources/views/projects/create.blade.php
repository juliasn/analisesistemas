@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h3>Novo Projeto</h3>
        </div>
        @if ($errors->any())
            <div class="row justify-content-center">
                <div class="col-4 alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @elseif(session('status'))
            <div class="row justify-content-center">
                <div class="col-4 alert alert-success">
                    {{ session('status') }}
                </div>
            </div>
        @endif
        <div class="row justify-content-center">
            <form class="col-4" method="POST" action="{{ route('projects.store') }}">
                @csrf
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nome para sua ideia" autocomplete="off" value="{{ old('name') }}">
                    <small id="nomeHelp" class="form-text text-muted">Mínimo de 4 caracteres.</small>
                </div>
                <div class="form-group">
                    <label for="description">Descrição</label>
                    <textarea class="form-control" id="description" name="description" placeholder="Descreva sua ideia">{{ old('description') }}</textarea>
                    <small id="nomeHelp" class="form-text text-muted">Mínimo de 10 caracteres.</small>
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="nao iniciado" {{ old('status') === 'nao iniciado' ? 'selected' : ''  }}>Não iniciado</option>
                        <option value="andamento" {{ old('status') === 'andamento' ? 'selected' : ''  }}>Andamento</option>
                        <option value="concluido" {{ old('status') === 'concluido' ? 'selected' : ''  }}>Concluído</option>
                        <option value="pausado" {{ old('status') === 'pausado' ? 'selected' : ''  }}>Pausado</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="type">Tipo</label>
                    <select name="type" id="type" class="form-control">
                        <option value="TCC" {{ old('type') === 'TCC' ? 'selected' : ''  }}>TCC</option>
                        <option value="ARTIGO" {{ old('type') === 'ARTIGO' ? 'selected' : ''  }}>Artigo</option>
                        <option value="PROJETO CIENTIFICO" {{ old('type') === 'PROJETO CIENTIFICO' ? 'selected' : ''  }}>Projeto Cientifico</option>
                        <option value="PESQUISA ACADEMICA" {{ old('type') === 'PESQUISA ACADEMICA' ? 'selected' : ''  }}>Pesquisa acadêmica</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="private">Visibilidade</label>
                    <select name="private" id="private" class="form-control">
                        <option value="PUBLIC" {{ old('private') === 'PUBLIC' ? 'selected' : ''  }}>Público</option>
                        <option value="PRIVATE" {{ old('private') === 'PRIVATE' ? 'selected' : ''  }}>Privado</option>
                    </select>
                </div>
                <input type="hidden" name="user_id" value="{{ auth()->user()->getAuthIdentifier() }}">
                <button type="submit" class="btn btn-primary btn-block">Criar</button>
            </form>
        </div>
    </div>
@endsection