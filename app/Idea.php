<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idea extends Model
{
    protected $fillable = ['name', 'description', 'status', 'project_type', 'private'];
    protected $hidden = ['id'];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
